const productsContainer = document.getElementById('products');

let shouldMakeRequest = true;
let limit = 4;
let skip = 0;
let total = 0;

fetchProductsAndRender();

window.addEventListener('scroll', handleScroll);

function handleScroll() {
  if (!shouldMakeRequest) return;

  const scrollHeight = document.documentElement.scrollHeight;
  const clientHeight = document.documentElement.clientHeight;
  const scrollTop = window.scrollY;

  const needToFetchNewItems = clientHeight + scrollTop >= scrollHeight;
  if (!needToFetchNewItems) return;

  skip += limit;
  fetchProductsAndRender();

  if (skip + limit >= total) {
    window.removeEventListener('scroll', handleScroll);
  }
}

async function fetchProductsAndRender() {
  shouldMakeRequest = false;

  const response = await fetch(
    `https://dummyjson.com/products?limit=${limit}&skip=${skip}`
  );
  const { products, total: totalFromResponse } = await response.json();

  shouldMakeRequest = true;

  total = totalFromResponse;

  let html = '';

  products.forEach((product) => {
    html += getProductHTML(product);
  });

  productsContainer.insertAdjacentHTML('beforeend', html);
}

function getProductHTML(product) {
  return `<div class="product">
    <img src="${product.thumbnail}" />
    <h2>${product.title}</h2>
    <p>${product.description}</p>
  </div>`;
}
