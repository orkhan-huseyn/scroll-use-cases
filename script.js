const boxes = document.querySelectorAll('.box');

window.addEventListener('scroll', function () {
  const windowHeight = document.documentElement.clientHeight;

  boxes.forEach((box) => {
    const top = box.getBoundingClientRect().top;
    const isIntersecting = top + 200 < windowHeight;
    if (isIntersecting) {
      box.classList.add('show');
    } else {
      box.classList.remove('show');
    }
  });
});
